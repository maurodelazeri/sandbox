version: "2"
services:
# Influx section
  influxdb:
    image: influxdb:latest
    restart: always
    volumes:
      - ./influxdb/data:/var/lib/influxdb
      - ./influxdb/config/:/etc/influxdb
    environment:
      INFLUXDB_DB: "westeros"
    ports:
      - "8086:8086"
      - "8082:8082"
      - "8089:8089"

  telegraf:
    image: telegraf:latest
    restart: always
    environment:
      HOSTNAME: "telegraf-getting-started"
    links:
      - influxdb
    volumes:
      - ./telegraf/:/etc/telegraf/
      - /var/run/docker.sock:/var/run/docker.sock
    depends_on:
      - influxdb

  kapacitor:
    image: kapacitor:latest
    restart: always
    volumes:
      - ./kapacitor/data/:/var/lib/kapacitor
      - ./kapacitor/config/:/etc/kapacitor/
    links:
      - influxdb
    ports:
      - "8092:8092"

  chronograf:
    build:
      context: ./images/chronograf/
      dockerfile: ./Dockerfile
    image: chrono_config
    restart: always
    environment:
      RESOURCES_PATH: "/usr/share/chronograf/resources"
    volumes:
      - ./chronograf/data/:/var/lib/chronograf/
    links:
      - influxdb
      - kapacitor
    ports:
      - "8888:8888"
    depends_on:
      - kapacitor
      - influxdb
      - telegraf

  ifql:
    build:
      context: ./images/ifqld_wait/
      dockerfile: ./Dockerfile
    image: ifqld_wait
    restart: always
    environment:
      - "HOSTS=influxdb:8082"
    ports:
      - "8093:8093"
    links:
      - influxdb
    depends_on:
      - influxdb

  grafana:
    image: grafana/grafana:latest
    restart: always
    ports:
    - "3000:3000"
    volumes:
    - ./grafana:/var/lib/grafana
    user: "104"
    links:
    - influxdb

# Kafka Section
  zoo1:
    image: zookeeper:3.4.9
    restart: always
    hostname: zoo1
    ports:
      - "2181:2181"
    environment:
        ZOO_MY_ID: 1
        ZOO_PORT: 2181
        ZOO_SERVERS: server.1=zoo1:2888:3888 server.2=zoo2:2888:3888 server.3=zoo3:2888:3888
    volumes:
      - ./zk-multiple-kafka-multiple/zoo1/data:/data
      - ./zk-multiple-kafka-multiple/zoo1/datalog:/datalog
      
  zoo2:
    image: zookeeper:3.4.9
    restart: always
    hostname: zoo2
    ports:
      - "2182:2182"
    environment:
        ZOO_MY_ID: 2
        ZOO_PORT: 2182
        ZOO_SERVERS: server.1=zoo1:2888:3888 server.2=zoo2:2888:3888 server.3=zoo3:2888:3888
    volumes:
      - ./zk-multiple-kafka-multiple/zoo2/data:/data
      - ./zk-multiple-kafka-multiple/zoo2/datalog:/datalog

  zoo3:
    image: zookeeper:3.4.9
    restart: always
    hostname: zoo3
    ports:
      - "2183:2183"
    environment:
        ZOO_MY_ID: 3
        ZOO_PORT: 2183
        ZOO_SERVERS: server.1=zoo1:2888:3888 server.2=zoo2:2888:3888 server.3=zoo3:2888:3888
    volumes:
      - ./zk-multiple-kafka-multiple/zoo3/data:/data
      - ./zk-multiple-kafka-multiple/zoo3/datalog:/datalog

  kafka1:
    image: confluentinc/cp-kafka:4.1.0
    hostname: kafka1
    restart: always
    ports:
      - "9092:9092"
    environment:
      # add the entry "127.0.0.1    kafka1" to your /etc/hosts file
      KAFKA_ADVERTISED_LISTENERS: "PLAINTEXT://kafka1:9092"
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181,zoo2:2182,zoo3:2183"
      KAFKA_BROKER_ID: 1
      KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
      KAFKA_LOG_RETENTION_HOURS: 72
    volumes:
      - ./zk-multiple-kafka-multiple/kafka1/data:/var/lib/kafka/data
    depends_on:
      - zoo1
      - zoo2
      - zoo3

  kafka2:
    image: confluentinc/cp-kafka:4.1.0
    restart: always
    hostname: kafka2
    ports:
      - "9093:9093"
    environment:
      # add the entry "127.0.0.1    kafka1" to your /etc/hosts file
      KAFKA_ADVERTISED_LISTENERS: "PLAINTEXT://kafka2:9093"
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181,zoo2:2182,zoo3:2183"
      KAFKA_BROKER_ID: 2
      KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
      KAFKA_LOG_RETENTION_HOURS: 72
    volumes:
      - ./zk-multiple-kafka-multiple/kafka2/data:/var/lib/kafka/data
    depends_on:
      - zoo1
      - zoo2
      - zoo3

  kafka3:
    image: confluentinc/cp-kafka:4.1.0
    restart: always
    hostname: kafka3
    ports:
      - "9094:9094"
    environment:
      # add the entry "127.0.0.1    kafka1" to your /etc/hosts file
      KAFKA_ADVERTISED_LISTENERS: "PLAINTEXT://kafka3:9094"
      KAFKA_ZOOKEEPER_CONNECT: "zoo1:2181,zoo2:2182,zoo3:2183"
      KAFKA_BROKER_ID: 3
      KAFKA_LOG4J_LOGGERS: "kafka.controller=INFO,kafka.producer.async.DefaultEventHandler=INFO,state.change.logger=INFO"
      KAFKA_LOG_RETENTION_HOURS: 72
    volumes:
      - ./zk-multiple-kafka-multiple/kafka3/data:/var/lib/kafka/data
    depends_on:
      - zoo1
      - zoo2
      - zoo3

# Mysql Section
  mysql:
    image: mysql:5.7
    restart: always
    ports:
      - "3307:3306"
    volumes:
      - ./mysql/data/:/var/lib/mysql
      - ./mysql/config/:/etc/mysql/mysql.conf.d
      - ./mysql/docker-entrypoint-initdb.d/:/docker-entrypoint-initdb.d
    environment:
      - MYSQL_ROOT_PASSWORD=cSKvHM2aDVu8nrak
      - MYSQL_DATABASE=zinnion


  # phpmyadmin:
  #   image: phpmyadmin/phpmyadmin:latest
  #   restart: always
  #   links:
  #     - mysql
  #   ports:
  #     - 8085:80
  #   environment:
  #     - PMA_ARBITRARY=1      

  #Redis Section
  redis:
    image: sameersbn/redis:latest
    restart: always
    ports:
    - "6379:6379"
    volumes:
    - ./redis/data/:/var/lib/redis
    environment:      
      EXTRA_ARGS: "--protected-mode no"
    restart: always

  # Westeros Streaming Section
  streaming:
    build:
      context: ./images/westeros/
      dockerfile: ./Dockerfile
    restart: always      
    ports:
      - "5050:5050"
    working_dir: /go/src/github.com/maurodelazeri/westeros
    command: /app/westeros
    environment:      
      MYSQLUSER: "root"
      MYSQLPASS: "cSKvHM2aDVu8nrak"
      DB_HOST: "mysql:3307"
      REDIS: "redis:6379"
      BROKERS: "kafka1:9092,kafka2:9093,kafka3:9094"   
      INFLUXHOST: "influxdb"
      TELEGRAMAPI: "552268599:AAHBd9PEyiQ5XB_o5DNBU8gp_kRArXDt8ms"
      TELEGRAMGROUP: "-265140096"
      MODE: "streaming"
      SLEEP: "true"
    links:
      - mysql
      - influxdb
      - kafka1
      - kafka2
      - kafka3
    depends_on:
      - mysql
      - influxdb
      - kafka1
      - kafka2
      - kafka3

  # Westeros Push Section
  push:
    build:
      context: ./images/westeros/
      dockerfile: ./Dockerfile
    restart: always      
    ports:
      - "2020:2020"
      - "2021:2021"
      - "2022:2022"
      - "2023:2023"
    working_dir: /go/src/github.com/maurodelazeri/westeros
    command: /app/westeros
    environment:      
      MYSQLUSER: "root"
      MYSQLPASS: "cSKvHM2aDVu8nrak"
      DB_HOST: "mysql:3307"
      TELEGRAMAPI: "552268599:AAHBd9PEyiQ5XB_o5DNBU8gp_kRArXDt8ms"
      TELEGRAMGROUP: "-265140096"
      MODE: "push"
      PUSH_GDAX_BIND: "tcp://:2020"
      PUSH_BITFINEX_BIND: "tcp://:2021"
      PUSH_BINANCE_BIND: "tcp://:2022"
      PUSH_BITMEX_BIND: "tcp://:2023"
      LOCAL_INTERFACE: "192.168.1.10"
      SLEEP: "true"
    links:
      - mysql
    depends_on:
      - mysql