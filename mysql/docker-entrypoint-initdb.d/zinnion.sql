CREATE TABLE `exchange` (
  `id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `name` text COLLATE utf8_unicode_ci,
  `maker` double DEFAULT NULL,
  `taker` double DEFAULT NULL,
  `verbose` tinyint(1) NOT NULL DEFAULT '0',
  `websocket` tinyint(1) NOT NULL DEFAULT '0',
  `kafka_streaming` tinyint(1) NOT NULL DEFAULT '1',
  `push_server` tinyint(1) NOT NULL DEFAULT '0',
  `execution` tinyint(1) NOT NULL DEFAULT '1',
  `authenticated_api_support` tinyint(1) NOT NULL DEFAULT '0',
  `api_key` text COLLATE utf8_unicode_ci NOT NULL,
  `api_secret` text COLLATE utf8_unicode_ci NOT NULL,
  `clientID` int(11) NOT NULL,
  `passphrase` text COLLATE utf8_unicode_ci NOT NULL,
  `basecurrencies` text COLLATE utf8_unicode_ci NOT NULL,
  `assettypes` text COLLATE utf8_unicode_ci NOT NULL,
  `markets` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `exchange` (`id`, `enabled`, `name`, `maker`, `taker`, `verbose`, `websocket`, `kafka_streaming`, `push_server`, `execution`, `authenticated_api_support`, `api_key`, `api_secret`, `clientID`, `passphrase`, `basecurrencies`, `assettypes`, `markets`) VALUES
(4, 1, 'binance', 0.0005, 0.0005, 0, 1, 1, 1, 1, 1, '0QChtwhMz4h9JxVwQchiebfZHFHqjpMOAFAhYfSfnxJcM5eaFtl4wKMlQsMU425I', 'TJ8wnY67n0n2FlARHobqt83rMoqEsUbfX3orGmBf2MBjM6spoibKvk2Yn1P2ahLw', 0, '', 'USD', 'SPOT', 'BTC,USD,ETH'),
(5, 1, 'gdax', 0, 0.003, 0, 1, 1, 1, 1, 1, '', '', 0, '', 'USD', 'SPOT', 'BTC,USD,ETH'),
(6, 1, 'bitfinex', 0.001, 0.002, 0, 1, 1, 1, 1, 1, 'a5lOk5noHwqbbMGmL2VdLDVSn6wNPDd4LF0MpUnZnyw', 'MIlsQ6OdGrOJUYIGYlEtb4aNmaQzQlX6Q4NyJovwdAF', 0, '', 'USD', 'SPOT', 'BTC,ETH,USD'),
(40, 1, 'bitmex', 0.0025, 0.0075, 0, 1, 1, 1, 1, 1, '', '', 0, '', 'USD', 'FUTURES', 'BTC,USD,ETH');

CREATE TABLE `exchange_pair` (
  `id` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `exchange_id` int(11) NOT NULL,
  `pair` text COLLATE utf8_unicode_ci,
  `pair_exchange` text COLLATE utf8_unicode_ci,
  `websocket_dedicated` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_order_size` float NOT NULL,
  `step_size` float NOT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `exchange_pair` (`id`, `enabled`, `exchange_id`, `pair`, `pair_exchange`, `websocket_dedicated`, `minimum_order_size`, `step_size`, `updated_at`) VALUES
(6057, 1, 40, 'BTC/USD', 'XBTUSD', 1, 0, 0, '2018-05-01 11:28:14'),
(6209, 0, 5, 'BCH/BTC', 'BCH-BTC', 0, 0.01, 0.00001, '2018-05-01 11:30:37'),
(6210, 1, 5, 'BCH/USD', 'BCH-USD', 0, 0.01, 0.01, '2018-05-01 11:30:37'),
(6211, 1, 5, 'BTC/USD', 'BTC-USD', 1, 0.001, 0.01, '2018-05-01 11:30:37'),
(6212, 0, 5, 'ETH/BTC', 'ETH-BTC', 0, 0.01, 0.00001, '2018-05-01 11:30:37'),
(6213, 1, 5, 'ETH/USD', 'ETH-USD', 1, 0.01, 0.01, '2018-05-01 11:30:37'),
(6214, 0, 5, 'LTC/BTC', 'LTC-BTC', 0, 0.1, 0.00001, '2018-05-01 11:30:37'),
(6215, 1, 5, 'LTC/USD', 'LTC-USD', 0, 0.1, 0.01, '2018-05-01 11:30:37'),
(6216, 1, 6, 'BTC/USD', 'BTCUSD', 1, 0.002, 0.00001, '2018-05-01 11:30:46'),
(6217, 0, 6, 'ETH/BTC', 'ETHBTC', 0, 0.02, 0.00001, '2018-05-01 11:30:46'),
(6218, 1, 6, 'BCH/USD', 'BCHUSD', 0, 0.02, 0.00001, '2018-05-01 11:30:46'),
(6219, 0, 6, 'BCH/BTC', 'BCHBTC', 0, 0.02, 0.00001, '2018-05-01 11:30:46'),
(6220, 0, 6, 'BCH/ETH', 'BCHETH', 0, 0.02, 0.00001, '2018-05-01 11:30:46'),
(6221, 1, 6, 'ETH/USD', 'ETHUSD', 1, 0.02, 0.00001, '2018-05-01 11:30:46'),
(6222, 0, 6, 'LTC/BTC', 'LTCBTC', 0, 0.08, 0.00001, '2018-05-01 11:30:46'),
(6223, 1, 6, 'LTC/USD', 'LTCUSD', 0, 0.08, 0.00001, '2018-05-01 11:30:46'),
(6224, 0, 6, 'XRP/BTC', 'XRPBTC', 0, 22, 0.00001, '2018-05-01 11:30:46'),
(6225, 0, 6, 'XRP/USD', 'XRPUSD', 0, 22, 0.00001, '2018-05-01 11:30:46'),
(6226, 0, 6, 'XMR/BTC', 'XMRBTC', 0, 22, 0.00001, '2018-05-01 11:30:46'),
(6227, 0, 6, 'XMR/USD', 'XMRUSD', 0, 0.06, 0.00001, '2018-05-01 11:30:46'),
(6228, 0, 6, 'NEO/BTC', 'NEOBTC', 0, 0.2, 0.00001, '2018-05-01 11:30:46'),
(6229, 0, 6, 'NEO/ETH', 'NEOETH', 0, 0.2, 0.00001, '2018-05-01 11:30:46'),
(6230, 0, 6, 'NEO/USD', 'NEOUSD', 0, 0.2, 0.00001, '2018-05-01 11:30:46'),
(6243, 1, 4, 'BTC/USD', 'BTCUSDT', 1, 0.000001, 0.000001, '2018-05-01 11:31:02'),
(6244, 0, 4, 'ETH/BTC', 'ETHBTC', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6245, 1, 4, 'BCH/USD', 'BCCUSDT', 0, 0.00001, 0.00001, '2018-05-01 11:31:02'),
(6246, 0, 4, 'BCH/BTC', 'BCCBTC', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6247, 0, 4, 'BCH/ETH', 'BCCETH', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6248, 1, 4, 'ETH/USD', 'ETHUSDT', 1, 0.00001, 0.00001, '2018-05-01 11:31:02'),
(6249, 0, 4, 'LTC/BTC', 'LTCBTC', 0, 0.01, 0.01, '2018-05-01 11:31:02'),
(6250, 0, 4, 'LTC/ETH', 'LTCETH', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6251, 1, 4, 'LTC/USD', 'LTCUSDT', 0, 0.00001, 0.00001, '2018-05-01 11:31:02'),
(6252, 0, 4, 'XRP/BTC', 'XRPBTC', 0, 1, 1, '2018-05-01 11:31:02'),
(6253, 0, 4, 'XRP/ETH', 'XRPETH', 0, 1, 1, '2018-05-01 11:31:02'),
(6254, 0, 4, 'XMR/BTC', 'XMRBTC', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6255, 0, 4, 'XMR/ETH', 'XMRETH', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6256, 0, 4, 'NEO/USD', 'NEOUSDT', 0, 0.001, 0.001, '2018-05-01 11:31:02'),
(6257, 0, 4, 'NEO/ETH', 'NEOETH', 0, 0.01, 0.01, '2018-05-01 11:31:02'),
(6258, 0, 4, 'NEO/BTC', 'NEOBTC', 0, 0.01, 0.01, '2018-05-01 11:31:02'),
(6262, 0, 4, 'ZEC/ETH', 'ZECETH', 0, 0.001, 0.001, '2018-05-18 01:43:39'),
(6263, 0, 4, 'ZEC/BTC', 'ZECBTC', 0, 0.001, 0.001, '2018-05-18 01:43:39'),
(6264, 0, 6, 'ZEC/ETH', 'ZECETH', 0, 0.001, 0.001, '2018-05-18 01:45:46'),
(6265, 0, 6, 'ZEC/BTC', 'ZECBTC', 0, 0.001, 0.001, '2018-05-18 01:45:46');


ALTER TABLE `exchange`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `exchange_pair`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_exchange_pair_1` (`exchange_id`);


ALTER TABLE `exchange`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
ALTER TABLE `exchange_pair`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6271;

ALTER TABLE `exchange_pair`
  ADD CONSTRAINT `fk_exchange_pair_1` FOREIGN KEY (`exchange_id`) REFERENCES `exchange` (`id`);

